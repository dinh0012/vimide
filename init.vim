for f in split(glob('~/.config/nvim/configs/*.vim'), '\n')
    exe 'source ~/.vimrc'
    exe 'source' f
endfor
